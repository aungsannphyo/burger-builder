import React from "react";
import Logo from "../../../assets/images/burger-logo.png";
import classess from "./Logo.css";

const logo = props => (
  <div className={classess.Logo}>
    <img src={Logo} alt="myburger" />
  </div>
);

export default logo;
