import React from "react";
import classess from "./Toolbar.css";
import Logo from "../../UI/Logo/logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import DrawerToggle from "../../Navigation/SideDrawer/DrawerToggle/DrawerToggle";

const toolbar = props => (
  <header className={classess.Toolbar}>
    <DrawerToggle clicked={props.drawerToggleClicked} />
    <div className={classess.Logo}>
      <Logo />
    </div>
    <nav className={classess.DesktopOnly}>
      <NavigationItems />
    </nav>
  </header>
);

export default toolbar;
